/* eslint-disable no-var */
/* global orangeCaptchaService */

document.addEventListener('DOMContentLoaded', function() {
    // Remove audio button if using Internet Explorer (he doesn't support WAV audio format)
    if (window.navigator.userAgent.indexOf('MSIE ') > 0 || !!window.navigator.userAgent.match(/Trident.*rv\:11\./)) {
        var $style = document.createElement('style');
        $style.innerHTML = '.orange_captcha_wrapper [data-action="switch"][data-type="AUDIO"] { display: none !important; }';
        document.querySelector('body').appendChild($style);
    }

    var $orangeCaptchaWrappers = document.querySelectorAll('.orange_captcha_wrapper');

    [].forEach.call($orangeCaptchaWrappers, function($orangeCaptchaContainer) {
        var instance = new OrangeCaptcha();
        instance.init($orangeCaptchaContainer);
    });
});

function OrangeCaptcha() {}

OrangeCaptcha.prototype.init = function($container) {
    this.$container = $container;
    this.$input_answer = this.$container.querySelector('input.orange_captcha_answer');
    this.$refresh_button = this.$container.querySelector('a[data-action="refresh"]');
    this.$buttons = this.$container.querySelectorAll('a');

    var _this = this;

    [].forEach.call(this.$buttons, function($button) {
        $button.addEventListener('click', function(e) {
            _this.clickAction(e);
        });
    });

    // Show default captcha
    this.$refresh_button.click();
};

OrangeCaptcha.prototype.clickAction = function(e) {
    e.preventDefault();
    this.$input_answer.value = '';
    this.$input_answer.className = '';

    var data = {
        action: 'get_orange_captcha',
        mode: e.target.dataset.action,
        type: (e.target.dataset.type || this.$container.dataset.type),
        locale: this.$container.dataset.locale,
    };

    var _this = this;

    this.ajaxPost(orangeCaptchaService.ajaxurl, data, function(response) {
        if (response.success) {
            [].forEach.call(_this.$container.querySelectorAll('.captcha .element'), function($element) {
                $element.classList.add('hidden');
            });

            var $parent = _this.$container.querySelector('.captcha .element[data-type="' + response.data.type + '"]');
            var $template = $parent.querySelector('template');
            var $element = $parent.querySelector('.element-container ' + (response.data.type === 'AUDIO' ? 'audio' : 'img'));

            if (response.data.type === 'AUDIO') {
                if (!$element) {
                    $parent.querySelector('.element-container').innerHTML = $template.innerHTML;
                    $element = $parent.querySelector('.element-container audio');
                }

                $element.setAttribute('src', response.data.element);
                $element.load();
            } else {
                if (!$element) {
                    $parent.querySelector('.element-container').innerHTML = $template.innerHTML;
                    $element = $parent.querySelector('.element-container img');
                }

                $element.setAttribute('src', response.data.element);
            }

            _this.$container.querySelector('.captcha .element[data-type="' + response.data.type + '"]').classList.remove('hidden');
            _this.$container.querySelector('input[name="orange_captcha[id]"]').value = response.data.id;

            if (response.data.switch) {
                _this.$container.setAttribute('data-type', response.data.type);
                _this.$container.querySelector('.controls a[data-action="switch"]').setAttribute('title', response.data.switch.title);
                _this.$container.querySelector('.controls a[data-action="switch"]').dataset.type = response.data.switch.type;
                _this.$container.querySelector('.controls a[data-action="switch"] img').setAttribute('src', response.data.switch.img);
            }

            _this.$container.classList.remove('hidden');
        }
    });
};

OrangeCaptcha.prototype.ajaxPost = function(url, data, onsuccess, onerror) {
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    request.withCredentials = true;

    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            var resp = JSON.parse(this.response);
            onsuccess(resp);
        } else {
            onerror(this);
        }
    };

    request.onerror = function() {
        onerror(this);
    };

    data = Object.keys(data).map(function(key) {
        return key + '=' + data[key];
    }).join('&');

    request.send(data);
};
