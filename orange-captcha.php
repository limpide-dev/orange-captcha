<?php
/**
* Plugin Name: Orange Captcha
* Description: Embed the Orange Captcha service.
* Version: 0.1
* Author: Pierre BREMARD
* Author URI: https://limpide.fr/
**/

class OrangeCaptchaServicePlugin {
    
    protected $version = 0.2;

    public function __construct() {
        
        add_action('wp_enqueue_scripts', array( $this, 'enqueue_scripts' ));
        add_action('init', array( $this, 'load_textdomain' ));

        add_shortcode( 'orange_captcha',  array( $this, 'shortcode' ) );
        add_action( 'wp_ajax_get_orange_captcha', array( $this, 'ajax' ) );
        add_action( 'wp_ajax_nopriv_get_orange_captcha', array( $this, 'ajax' ) );

        add_action( 'wpcf7_init', array( $this, 'wpcf7_add_form_tag_orange_captcha') );
        add_filter( 'wpcf7_validate_orange_captcha', array( $this, 'wpcf7_orange_captcha_validation_filter'), 10, 2 );
        add_filter( 'wpcf7_messages', array( $this, 'wpcf7_orange_captcha_messages') );

        add_action( 'wp_footer', array( $this, 'wpcf7_orange_captcha_refresh' ));
       
        if( is_admin() ) {
            $my_settings_page = new OrangeCaptchaServiceSettings();
        }
    }

    public function enqueue_scripts() {
        wp_enqueue_script( 'orange_captcha_service', plugin_dir_url( __FILE__ ) . 'assets/js/app.js', array(), $this->version, true);
        wp_localize_script( 'orange_captcha_service', 'orangeCaptchaService', array( 'ajaxurl' => admin_url( 'admin-ajax.php')));
        wp_enqueue_style( 'orange_captcha_service',  plugin_dir_url( __FILE__ ) . 'assets/css/style.css', array(), $this->version, 'all' );
    }
    
    public function load_textdomain() {
        load_plugin_textdomain('orange-captcha', false, plugin_basename(dirname(__FILE__)) . '/languages');
    }

    public static function get_orange_captcha_service() {
        $orange_captcha_service = new OrangeCaptchaService();
        return $orange_captcha_service;
    }

    private function get_orange_captcha_html($type) {
        $element_html = '<template id="orange-captcha-' . strtolower($type) . '">';

        if ($type === 'IMAGE') {
            $element_html .= '<img data-no-lazy="1" alt="">';
        } else if ($type === 'AUDIO') {
            $element_html .= '<audio controls><source data-no-lazy="1" type="audio/wav">Your browser does not support the audio element.</audio>';
        }

        $element_html .= '</template>';

        $element_html .= '<div class="element-container"></div>';

        return $element_html;
    }

    public function shortcode( $atts = [], $content = null) {

        if (!is_array($atts)) $atts = array();
        if (!isset($atts['type'])) $atts['type'] = 'IMAGE';

        // Force locale based on current language
        // Polylang

        if ( function_exists( 'pll_current_language' ) ) { 
            $atts['locale'] = pll_current_language();
        // WPML
        } else if (function_exists( 'icl_object_id' )) {
            $atts['locale'] = apply_filters( 'wpml_current_language', NULL );
        // WP
        } else {
            $atts['locale'] = substr(get_locale(),0,2);
        }
        
        $elements_type = array('IMAGE', 'AUDIO');
        $service = $this->get_orange_captcha_service();
        
        $content .= '<div class="orange_captcha_wrapper hidden '. ($atts['class'] ?? '') .'" data-type="' . $atts['type'] . '" data-locale="' . ($atts['locale'] ?? '') . '">';
        $content .= '<div class="captcha">';
        
        foreach ($elements_type as $type) {
            $content .='<div class="element '. ($type !== $atts['type'] ? 'hidden' : '') .'" data-type="' . $type . '">';
            $content .= $this->get_orange_captcha_html($type);
            $content .='</div>';
        }

        $array_values = array_values(array_diff($service->get_types(), array($atts['type'])));
        $other_element = array_shift($array_values);

        $content .='<div class="controls">';
        $content .= '<a href="" data-action="switch" title="' . __('Switch to', 'orange-captcha') . ' ' . strtolower($other_element). '" data-type="' . $other_element . '"><img data-no-lazy="1" src="' . plugin_dir_url( __FILE__ ) . 'assets/img/' . $other_element . '.svg"/></a>';
        $content .= '<a href="" title="' . __('Refresh', 'orange-captcha') . '" data-action="refresh"><img data-no-lazy="1" src="' . plugin_dir_url( __FILE__ ) . 'assets/img/refresh.svg"/></a>';
        $content .= '</div>';
        $content .= '</div>';
        $content .= '<div class="captcha-input">';
        $content .= '<span class="wpcf7-form-control-wrap orange_captcha">';
        $content .= '<input type="text" class="orange_captcha_answer" autocomplete="'. ($atts['autocomplete'] ?? 'off') .'" aria-invalid="'. ($atts['aria-invalid'] ?? 'false') .'" ' . (!isset($atts['required']) || $atts['required'] !== 'false' ? 'required' : '') . ' aria-required="'. ($atts['aria-required'] ?? 'false') .'" name="orange_captcha[answer]" placeholder="' . __('Please type the code above', 'orange-captcha') . '" />';
        $content .= '</span>';
        $content .= '<input type="hidden" class="orange_captcha_id" name="orange_captcha[id]" value=""/>';
        $content .= '</div>';
        $content .= '</div>';

        return $content;
    }

    public function ajax() {

        $service = $this->get_orange_captcha_service();
        $captcha = $service->create($_POST['type'], $_POST['locale']);
        $array_values = array_values(array_diff($service->get_types(), array($captcha->type)));
        $other_element = array_shift($array_values);

        $results = array(
            'id' => $captcha->id,
            'type' => $captcha->type,
            'element' => $captcha->element,
        );

        if ('switch' == $_POST["mode"]) {
            $results['switch'] = array(
                'img' => plugin_dir_url( __FILE__ ) . 'assets/img/' . $other_element . '.svg',
                'type' => $other_element,
                'title' => __('Switch to', 'orange-captcha') . ' ' . strtolower($other_element),
            );
        }

        wp_send_json_success($results);
    }
    
    public function wpcf7_add_form_tag_orange_captcha() {
        
        wpcf7_add_form_tag( 'orange_captcha',
            array( $this, 'wpcf7_orange_captcha_form_tag_handler'),
            array(
                'name-attr' => false,
                'zero-controls-container' => true,
                'not-for-mail' => true,
            )
        );
    }

    public function wpcf7_orange_captcha_form_tag_handler( $tag ) {

        if ( ! class_exists( 'OrangeCaptchaService' ) ) {
            $error = sprintf(
                esc_html( __( "To use Orange Captcha, you need %s plugin installed.", 'contact-form-7' ) ),
                wpcf7_link( 'https://limpide.fr', 'Orange Captcha' ) );

            return sprintf( '<em>%s</em>', $error );
        }
        
        $validation_error = wpcf7_get_validation_error( 'orange_captcha_service' );

        $class = wpcf7_form_controls_class( $tag->type );
        
        if ( $validation_error ) {
            $class .= ' wpcf7-not-valid';
        }

        if ( $tag->is_required() ) {
            $atts['aria-required'] = 'true';
        }
        
        
        $atts = array();
        $atts['class'] = $tag->get_class_option( $class );
        $atts['autocomplete'] = 'off';
        $atts['aria-invalid'] = $validation_error ? 'true' : 'false';
        $atts['error'] = $validation_error;

        $html = $this->shortcode($atts);

        return $html;
    }


    /* Validation filter */
    public function wpcf7_orange_captcha_validation_filter( $result, $tag ) {
        
        $service = $this->get_orange_captcha_service();
        $check_answer = $service->check_answer($_POST['orange_captcha']['id'], $_POST['orange_captcha']['answer']);
        
        if (!$check_answer) {
            $tag->name = 'orange_captcha';
            $result->invalidate( $tag, wpcf7_get_message( 'orange_captcha_not_match' ) );
        }
        
        return $result;
    }

    /* Refresh Orange Captcha if error - add in footer */

    function wpcf7_orange_captcha_refresh() {
    ?>
    <script type="text/javascript">
    document.addEventListener( 'wpcf7submit', function( event ) {
        switch ( event.detail.status ) {
            case 'spam':
            case 'mail_sent':
            case 'mail_failed':
            case 'validation_failed':
                document.querySelector('.orange_captcha_wrapper a[data-action="refresh"]').click();
        }
    }, false );
    </script>
    <?php
    }

    // TODO
    /*
    add_filter( 'wpcf7_spam', 'wpcf7_recaptcha_check_with_google', 9 );

    function wpcf7_recaptcha_check_with_google( $spam ) {
        if ( $spam ) {
            return $spam;
        }

        $contact_form = wpcf7_get_current_contact_form();

        if ( ! $contact_form ) {
            return $spam;
        }

        $tags = $contact_form->scan_form_tags( array( 'type' => 'recaptcha' ) );

        if ( empty( $tags ) ) {
            return $spam;
        }

        $recaptcha = WPCF7_RECAPTCHA::get_instance();

        if ( ! $recaptcha->is_active() ) {
            return $spam;
        }

        $response_token = wpcf7_recaptcha_response();
        $spam = ! $recaptcha->verify( $response_token );

        return $spam;
    }
    */

    /* Messages */

    function wpcf7_orange_captcha_messages( $messages ) {
        $messages = array_merge( $messages, array(
            'orange_captcha_not_match' => array(
                'description' =>
                    __( "The code that sender entered does not match the CAPTCHA", 'contact-form-7' ),
                'default' =>
                    __( 'Your entered code is incorrect.', 'contact-form-7' ),
            ),
        ) );

        return $messages;
    }
}

class OrangeCaptchaService {
    
    protected $option_id = 'orange_captcha_service';
    protected $options;
    protected $types = array('IMAGE','AUDIO');

    const ENDPOINT_CREATE       = '/public/backend/api/v2/captcha';
    const ENDPOINT_CHECK_ANSWER = '/public/backend/api/v2/captcha/{captcha_id}/checkAnswer';
    const ENDPOINT_GET          = '/public/frontend/api/v2/captcha/{captcha_id}.{extension}';
    const EXT_IMAGE             = 'png';
    const EXT_AUDIO             = 'wav';

    public function __construct() {
        $this->options = (object)(is_multisite() ? get_site_option($this->option_id) : get_option($this->option_id) );
    }

    public function request(string $endpoint, array $datas) {
       
        $url = $this->options->api_server . $endpoint;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, $this->options->client_id . ":" . $this->options->client_secret);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($datas));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $result = curl_exec($ch);
        curl_close($ch);
        
        $response = json_decode($result);
        
        return $response;
    }

    public function create(string $type = 'IMAGE', string $locale = 'fr') {

        if (empty($type)) $type = 'IMAGE';
        
        $datas = array(
            'type' => $type,
            'locale' => $locale,
        );

        try {
            $response = $this->request(
                self::ENDPOINT_CREATE, 
                $datas
            );
            
            if ($type == 'IMAGE') {
                $ext_type = self::EXT_IMAGE;
            } else if ($type == 'AUDIO') {
                $ext_type = self::EXT_AUDIO;
            }

            $captcha = array(
                'id' => $response->id,
                'type' => $type,
                'element' => str_replace(array('{captcha_id}', '{extension}'), array($response->id, $ext_type), $this->options->api_server . self::ENDPOINT_GET),
            );

            return (object) $captcha;

        } catch (\Exception $e) {
            // TODO
            print_r($e);
        }
        
    }

    public function check_answer(string $id, string $input) {

        $datas = array('answer'=>$input);
        
        try {

            $url = str_replace('{captcha_id}', $id, self::ENDPOINT_CHECK_ANSWER);
            
            $response = $this->request(
                $url, 
                $datas
            );

            if ($response && isset($response->result) && $response->result == "SUCCESS") {
                return true;
            } else {
                return false;
            }

        } catch (\Exception $e) {
            // TODO
        }
    }

    public function get_types() {
        return $this->types;
    }
}

class OrangeCaptchaServiceSettings
{
    protected $full_name = 'Orange Captcha Service';
    protected $identifier = 'orange-captcha-service-settings';
    protected $option_id = 'orange_captcha_service';
    protected $access_level;
    protected $save_url;
    protected $options;

    /**
     * Start up
     */
    public function __construct()
    {
        if (is_multisite()) {

            $this->access_level = 'manage_network_options';
            $this->save_url = 'edit.php?action=save_settings';

            add_action( 'network_admin_menu', array( $this, 'add_plugin_page' ) );
            add_action( 'network_admin_edit_save_settings',  array( $this, 'orange_captcha_save_settings' ) );
            
            // TODO
            
            /*
            add_action( 'network_admin_notices', array( $this, 'orange_captcha_notices') );
            */

            $this->options = get_site_option($this->option_id, array(
                'api_server' => '',
                'client_id' => '',
                'client_secret' => '',
            ));

        } else {

            $this->access_level = 'manage_options';
            $this->save_url = 'options.php';
            add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
            
            $this->options = get_option($this->option_id, array(
                'api_server' => '',
                'client_id' => '',
                'client_secret' => '',
            ));
        }
        
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        if (current_user_can($this->access_level)) {
            if (is_multisite()) {
                add_submenu_page(
                    'settings.php', // Parent element
                    $this->full_name, // Text in browser title bar
                    $this->full_name, // Text to be displayed in the menu.
                    $this->access_level, // Capability
                    $this->identifier, // Page slug, will be displayed in URL
                    array( $this, 'create_admin_page' )
                );
            } else {
                add_options_page(
                    $this->full_name, 
                    $this->full_name, 
                    $this->access_level, 
                    $this->identifier, 
                    array( $this, 'create_admin_page' )
                );
            }
        }
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        // $this->options = get_option( 'orange_captcha_service' );
        ?>
        <div class="wrap">
            <h1><?php echo $this->full_name;?></h1>
            <form method="post" action="<?=$this->save_url;?>">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'orange_captcha_service' );
                do_settings_sections( 'my-setting-admin' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {    
        
        register_setting(
            $this->option_id,
            $this->option_id
        );

        add_settings_section(
            'setting_section_id', // ID
            'Settings', // Title
            '', // Callback
            'my-setting-admin' // Page
        );  

        add_settings_field(
            'api_server', // ID
            'Api Server URL', // Title 
            array( $this, 'api_server_callback' ), // Callback
            'my-setting-admin', // Page
            'setting_section_id' // Section           
        );      

        add_settings_field(
            'client_id', 
            'Client Id', 
            array( $this, 'client_id_callback' ), 
            'my-setting-admin', 
            'setting_section_id'
        );
        
        add_settings_field(
            'client_secret', 
            'Client Secret', 
            array( $this, 'client_secret_callback' ), 
            'my-setting-admin', 
            'setting_section_id'
        );
    }
    

    public function orange_captcha_save_settings(){
        if (is_multisite()) {
            update_site_option( $this->option_id, $_POST[$this->option_id] );
            $redirect_url =  network_admin_url('settings.php');
        }

        wp_redirect( add_query_arg( array(
            'page' => $this->identifier,
            'updated' => true ), $redirect_url
        ));

        exit;
    }

    // TODO
    /*
    public function orange_captcha_custom_notices(){
 
        if( isset($_GET['page']) && $_GET['page'] == $this->identifier && isset( $_GET['updated'] )  ) {
            echo '<div id="message" class="updated notice is-dismissible"><p>Settings updated</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
        }
     
    }
    */

    /** 
     * Get the settings option array and print one of its values
     */
    public function api_server_callback()
    {
        printf(
            '<input type="text" id="api_server" class="regular-text" name="orange_captcha_service[api_server]" value="%s" />',
            isset( $this->options['api_server'] ) ? esc_attr( $this->options['api_server']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function client_id_callback()
    {
        printf(
            '<input type="text" id="client_id" class="regular-text" name="orange_captcha_service[client_id]" value="%s" />',
            isset( $this->options['client_id'] ) ? esc_attr( $this->options['client_id']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function client_secret_callback()
    {
        printf(
            '<input type="text" id="client_secret" class="regular-text" name="orange_captcha_service[client_secret]" value="%s" />',
            isset( $this->options['client_secret'] ) ? esc_attr( $this->options['client_secret']) : ''
        );
    }
}

new OrangeCaptchaServicePlugin();

?>